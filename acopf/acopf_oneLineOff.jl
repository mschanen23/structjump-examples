include("opfdata.jl")
include("acopf.jl")

function scopf_log_summary_line(case,line)
  f=open(case*"_SCOPF_summary", "a+")
  write(f, line*"\n")
  close(f)
end

function scopf_log_del(case)
  if isfile(case*"_SCOPF_summary"); rm(case*"_SCOPF_summary"); end
end


#
# MAIN 
#

opfdata = opf_loaddata(ARGS[1])
scopf_log_del(ARGS[1])


buses = opfdata.buses
NotRemovableLines=[]

scopf_log_summary_line(ARGS[1],"Lines that will not be considered since they will be islanding buses")

#find all load nodes that are connected by a single line
for b in buses
  #if b.Pd!=0 || b.Qd!=0
    if length(opfdata.ToLines[  opfdata.BusIdx[b.bus_i]])+length(opfdata.FromLines[opfdata.BusIdx[b.bus_i]])<=1
      print("Bus ", b.bus_i, " (index ", opfdata.BusIdx[b.bus_i], ") is connected only by one line: ")
      for l in opfdata.ToLines[  opfdata.BusIdx[b.bus_i]]
        print("Line(fromBus,toBus):(", opfdata.lines[l].to, ",", opfdata.lines[l].from,")  ")
        scopf_log_summary_line(ARGS[1], @sprintf("Line %5d: from,to=%5d,%5d", l, opfdata.lines[l].from, opfdata.lines[l].to)) 
        push!(NotRemovableLines,l)
      end
      for l in opfdata.FromLines[opfdata.BusIdx[b.bus_i]]
        print("Line(fromBus,toBus):(", opfdata.lines[l].to, ",", opfdata.lines[l].from,")  ")
        scopf_log_summary_line(ARGS[1], @sprintf("Line %5d: from,to=%5d,%5d", l, opfdata.lines[l].from, opfdata.lines[l].to)) 
        push!(NotRemovableLines,l)
      end
      println("")
    else
      #nothing for now
    end
  #end
end

RemovableLines=Array(Int,0)
sort!(NotRemovableLines)
for l in 1:length(opfdata.lines)
  if l in NotRemovableLines; continue; end
  push!(RemovableLines,l);
end

scopf_log_summary_line(ARGS[1],"Analyzing the system for each of the remaining lines...")
println("Analyzing the system for each of the remaining lines...")
for l in RemovableLines
  msg = @sprintf("Line %5d: from,to=%5d,%5d", l, opfdata.lines[l].from, opfdata.lines[l].to)
  println(msg)

  opfdata_1off = opf_loaddata(ARGS[1], opfdata.lines[l])
  opfmodel = acopf_model(opfdata_1off)
  opfmodel,status = acopf_solve(opfmodel,opfdata_1off)
  if status!=:Optimal
    println("Not optimal: ", status)
    msg = @sprintf("%s    %s", msg,  status)
  else
    println("Optimal: objective=", getobjectivevalue(opfmodel))
    msg = @sprintf("%s    optimal   objective: %15.8f", msg,  getobjectivevalue(opfmodel))
    #acopf_outputAll(opfmodel,opfdata_1off)
  end
  scopf_log_summary_line(ARGS[1], msg)
  println("--------------------------------------")
end