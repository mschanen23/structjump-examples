include("scopf_stochDemand.jl")

# to obtain a list of off-lines run:  grep "optimal" summaries/case30_SCOPF_summary | awk '{print $2;}' | tr ':\n' ' '


function main()
  if length(ARGS) < 3
    println("Usage: julia scopf_stcohDemain_main.jl case_name num_scens_per_conting lines_indexes")
    println("Cases are in 'data' directory: case9 case30 case57 case118 case300 case1354pegase case2383wp case2736sp case2737sop case2746wop case2869pegase case3012wp  case3120sp case3375wp case9241pegase")
    return
  end

  opfdata = opf_loaddata(ARGS[1])
  num_scens=parse(Int, ARGS[2])
  lines_off=Array{Line}(length(ARGS)-2)
  for l in 1:length(lines_off)
    lines_off[l] = opfdata.lines[parse(Int,ARGS[l+2])]
  end
  scopfdata = SCOPFData(opfdata,lines_off)
  scopfmodel = scopf_model(scopfdata, num_scens)
  opfmodel,status = scopf_solve(scopfmodel,scopfdata, num_scens)
  if status==:Optimal
    #acopf_outputAll(opfmodel,opfdata)
  end
end


main()
